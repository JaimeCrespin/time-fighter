package com.example.jaimecrespin.timefighter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast
import com.example.jaimecrespin.timefighter.R.layout.game_score
import com.google.firebase.database.FirebaseDatabase
import java.util.*



class GameMainActivity : AppCompatActivity() {
    internal lateinit var gameScoreTextView:TextView
    internal lateinit var timeleftTextView: TextView
    internal lateinit var tapButton: Button
    internal lateinit var scoreButton: Button

    internal var score = 0

    internal var gameStarted = false
    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown = 10000L
    internal val countDownInterval = 1000L
    internal var timeLeft = 10

    internal val TAG = GameMainActivity::class.java.simpleName
    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("timefighter")

    companion object {
        private  val SCORE_KEY = "SCORE_KEY"
        private val  TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_main)

        //connect view to variables
        gameScoreTextView = findViewById(R.id.puntaje_Juego_text_view)
        timeleftTextView = findViewById(R.id.tiempo_Juego_text_view)
        tapButton = findViewById(R.id.boton_Presioname)
        scoreButton= findViewById(R.id.scorebtn)


        if(savedInstanceState != null) {
            score = savedInstanceState.getInt(SCORE_KEY)
            timeLeft = savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        } else {
            resetGame()
        }

        tapButton.setOnClickListener{ _  -> incrementScore()}
        scoreButton.setOnClickListener{ _ -> showScore()}



    }
    private fun showScore() {
        val listScore = Intent(this, GameScore::class.java)
        startActivity(listScore)

    }
    private fun resetGame() {
        score = 0
        timeLeft = 10

        val  gameScore = getString(R.string.mi_puntaje, Integer.toString(score))
        gameScoreTextView.text = gameScore

        val timeLeftText = getString(R.string.tiempo_restante, Integer.toString(timeLeft))
        timeleftTextView.text = timeLeftText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeleftTextView.text = getString(R.string.tiempo_restante, Integer.toString(timeLeft))
            }

            override fun onFinish() {
                endGame()
            }
        }
        gameStarted = false

    }

    private fun incrementScore() {

        score ++

        //val newScore = "Tu Puntaje: "+ Integer.toString(score)
        val newScore = getString(R.string.mi_puntaje,Integer.toString(score))
        gameScoreTextView.text = newScore

        if(!gameStarted){
            startGame()
        }

    }

    private fun startGame() {
        countDownTimer.start()
        gameStarted = true

    }

    private fun endGame() {
        ref.child(UUID.randomUUID().toString()).child("Score").setValue(Integer.toString(score))
        Toast.makeText(this, getString(R.string.game_over_message,Integer.toString(score)), Toast.LENGTH_LONG).show()
        resetGame()
    }
    private fun restoreGame(){
        val restoredScore = getString(R.string.mi_puntaje,Integer.toString(score))
        gameScoreTextView.text = restoredScore

        val restoredTime = getString(R.string.tiempo_restante,Integer.toString(timeLeft))
        timeleftTextView.text = restoredTime

        countDownTimer = object : CountDownTimer(timeLeft*1000L,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {

                timeLeft= millisUntilFinished.toInt()/ 1000

                timeleftTextView.text = getString(R.string.tiempo_restante,timeLeft.toString())
            }

            override fun onFinish() {
                endGame()
            }
        }
        countDownTimer.start()
        gameStarted = true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putInt(SCORE_KEY,score)
        outState!!.putInt(TIME_LEFT_KEY,timeLeft)
        countDownTimer.cancel()
        Log.d(TAG, "onSaveInstance State: score = $score & timeleft = $timeLeft" )
    }

    override fun onDestroy() {
        super.onDestroy()

        Log.d(TAG,"onDestroy called")
    }
}
