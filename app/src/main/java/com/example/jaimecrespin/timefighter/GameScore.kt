package com.example.jaimecrespin.timefighter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase



class GameScore : AppCompatActivity() {

    val database = FirebaseDatabase.getInstance()
    val ref = database.getReference("timefighter")
    lateinit var listsRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_score)

        // RecyclerView

        listsRecyclerView = findViewById(R.id.list_score_view)
        listsRecyclerView.layoutManager = LinearLayoutManager(this)
        listsRecyclerView.adapter = ListSelectionRecyclerViewAdapter(ref)


    }
}



