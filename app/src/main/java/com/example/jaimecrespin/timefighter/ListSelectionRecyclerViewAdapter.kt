package com.example.jaimecrespin.timefighter


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import java.util.*


class ListSelectionRecyclerViewAdapter(ref:DatabaseReference):
        RecyclerView.Adapter<ListSelectionRecyclerViewHolder>() {


    val listTitles:ArrayList<String> = arrayListOf()

    init {

        ref.addChildEventListener(object : ChildEventListener {

            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val listTitle = item.child("Score").value.toString()
                listTitles.add(listTitle)
                notifyDataSetChanged()

            }

            override fun onChildRemoved(item: DataSnapshot) {
            }
        })

    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): ListSelectionRecyclerViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.selection_holder, parent, false)

        return ListSelectionRecyclerViewHolder(view)

    }


    override fun getItemCount(): Int {
        return listTitles.count()
    }


    override fun onBindViewHolder(
            holder: ListSelectionRecyclerViewHolder,
            position: Int) {

        holder.listTitle.text = listTitles[position]
    }
}


